const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'blog',
  {
    idBlog: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title:{type:Sequelize.STRING},
    mContent:{type:Sequelize.STRING},
    fContent:{type:Sequelize.STRING},
    Photo:{type:Sequelize.STRING}
  },
  {
    timestamps: false
  }
)