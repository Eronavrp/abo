import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios'
import style from './Home.module.css';
import Article from './components/Article';
import Leaderboard from './components/Leaderboard';
import Footer from './../Component/Footer';
import logo from './../../Images/logo_white.png';
export default class Home extends Component {
  constructor() {
    super()
    this.state = {
      lista:[]
    }
  }  
  componentDidMount() {
    axios.get('http://localhost:8080/blogs/blog').then(res=>{
      const info = res.data; 
      this.setState({ lista:info });
    })
  }

  render() {
    return (
        <div id={style.main}>
          <div className={`container-fluid pb-4  ${style.kuq}`} > 
            <img src={logo} alt='' id={style.header}/>
          </div>
           {/* <img src={heartBeat} alt='' className={`${style.Beat}`}/> */}
          <Leaderboard/> 
            {this.state.lista.map( (item,key) => <Article key={item.idBlog.toString()} Titull={item.title} MainC={item.mContent} Contenti={item.fContent} />)}
          <Footer/>
        </div>                 
    ) 
  }
}
