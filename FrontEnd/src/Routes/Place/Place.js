import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import style from './Place.module.css'
import ReactMapGL from 'react-map-gl';
import Footer from './../Component/Footer';

export default class Place extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        latitude: 42.645193770772025,
        longitude: 21.16160511692998,
        zoom: 14,
        bearing: 0,
        pitch: 0
      }
    };
  }

  render() {
    const { data } = this.props.location
    return (
      <div>
        <div style={{ margin: '2.5%' }}>
          <ReactMapGL
            {...this.state.viewport}
            width="95vw"
            height="80vh"
            mapStyle="mapbox://styles/notelbion/ck6z052pw2bzo1jryhg1bojo0"
            onViewportChange={viewport => this.setState({ viewport })}
            mapboxApiAccessToken='pk.eyJ1Ijoibm90ZWxiaW9uIiwiYSI6ImNqdDF3b2ZsbzBmMjk0YnAzcWIwaW9jc2UifQ.VsQ_49cngtGm9kApyXFCkw'
          />
          <Link to='/home'> <button className={style.doneButton}> Done </button> </Link>
        </div>
      <Footer/>
      </div>
    )
  }
  
}