import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import style from './Donate.module.css';
import photo from '../../Images/hospitals/qkuk.jpg'

export default class DonateComp extends Component {
  render() {
    return (
      <div class="card" id={`${style.cardi}`} >
  					<img src={this.props.photo} class="card-img-top" id={style.pho} alt="..." />
  					<div class="card-body text-center" id={`${style.cardB}`}>
    					<h5 class="card-title">{this.props.hospital}</h5>
    					<p class="card-text" id="txti">We need <span id={this.props.blood}>{this.props.blood}</span>  blood for our hospital. <br/> 
    					If you are intrested click the donate button</p><Link to={{pathname:`/calendar/${this.props.hospital}/${this.props.blood}`,data:this.props.hospital }}><p className="btn btn-primary" id={style.nextbtn}>Donate</p></Link>
  					</div>
				</div>
    )
  }
}