import React, { Component } from "react";
import style from "./Leaderboard.module.css";
import LeaderboardPosition from './Leaderboard_position'
import Trophy from "./../../../Images/trophy.png"

export default class Leaderboard extends Component {
  render() {
    return (
    
      <table className="table tabel-sm">
          <div className={`container`}> 
          <img src={Trophy} alt='' id={style.trophy}/>
         </div>
        <tbody>
        <LeaderboardPosition nr='1' name='Veton Klinaku' points='2000pt' />
        <LeaderboardPosition nr='2' name='Rrezon Pllana' points='1800pt' />
        <LeaderboardPosition nr='3' name='Altina Kasabaqi' points='1600pt' />
        <LeaderboardPosition nr='4' name='Erona Vrapcani' points='1300pt' />
        </tbody>
    </table>
     
    );
  }
}