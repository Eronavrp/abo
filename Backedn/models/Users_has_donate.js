const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'users_has_donate',
  {
    Users_idUsers: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    Users_Appointments_idAppointments:{type:Sequelize.INTEGER},
    Donate_idDonate:{type:Sequelize.INTEGER},
  },
  {
    timestamps: false
  }
)