import React, { Component } from 'react'
import { register } from './../../API/UserFunctions'
import style from './Signup.module.css'

class Register extends Component {
  constructor() {
    super()
    this.state = {
      Name: '',
      Surname: '',
      Email: '',
      Password: '',
      Gender:'',
      Blood:'',
      Addres:'',
      MedicalIssues:'',
      errors: {}
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()

    const newUser = {
      Name: this.state.Name,
      Surname: this.state.Surname,
      Email: this.state.Email,
      Password: this.state.Password,
      Gender: this.state.Gender,
      Blood: this.state.Blood,
      Addres: this.state.Addres,
      MedicalIssues: this.state.MedicalIssues,
    }

    register(newUser).then(res => {
      this.props.history.push(`/login`)
    })
  }

  render() {
    return (
          <div className="p-5" id={`${style.mainS}`}>
            <form onSubmit={this.onSubmit} className="text-white">
              <h1 className="h3 mb-3 font-weight-normal ">Register</h1>

              <div className="form-group">              
                <label  htmlFor="Name">First name</label>
                <input
                  required
                  type="text"
                  className={`form-control ${style.inp}`}
                  name="Name"
                  value={this.state.Name}
                  onChange={this.onChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="Surname">Last name</label>
                <input
                  required
                  type="text"
                  className={`form-control ${style.inp}`}
                  name="Surname"
                  value={this.state.Surname}
                  onChange={this.onChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="Email">Email address</label>
                <input
                  required
                  type="text"
                  className={`form-control ${style.inp}`}
                  name="Email"
                  value={this.state.Email}
                  onChange={this.onChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="Password">Password</label>
                <input
                  required
                  type="password"
                  className={`form-control ${style.inp}`}
                  name="Password"
                  value={this.state.Password}
                  onChange={this.onChange}
                />
              </div>

              <div className="d-flex flex-column ">
                <div>
                  <label htmlFor="Gender">Gender</label>
                </div>
                <div className="d-flex mx-auto ">
                  <div className="form-check ">
                    <label>
                      <input
                        type="radio"
                        name="Gender"
                        value="Male"
                        checked={this.state.Gender === "Male"}
                        onChange={this.onChange}
                        className="form-check-input"
                      />
                      Male
                    </label>
                  </div>
                  <div className="form-check ml-4">
                    <label>
                      <input
                        type="radio"
                        name="Gender"
                        value="Female"
                        checked={this.state.Gender === "Female"}
                        onChange={this.onChange}
                        className="form-check-input"
                      />
                      Female
                    </label>
                  </div>
                </div>
              </div>

              <div className="form-group d-flex flex-column ">
                <div>
                  <label htmlFor="Blood">Blood Group</label>
                </div>
                <div>
                  <select className={`form-control ${style.inp}`} name="Blood" value={this.state.Blood} onChange={this.onChange}>
                    <option value="0+">0+</option>
                    <option value="0-">0-</option>
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B-">B-</option>
                    <option value="AB+">AB+</option>
                    <option value="AB-">AB-</option>
                  </select>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="Addres">Addres</label>
                <input
                  required  
                  type="text"
                  className={`form-control ${style.inp}`}
                  name="Addres"
                  value={this.state.Addres}
                  onChange={this.onChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="MedicalIssues">Medical Issues</label>
                <input
                  required  
                  type="text"
                  className={`form-control ${style.inp}`}
                  name="MedicalIssues"
                  value={this.state.MedicalIssues}
                  onChange={this.onChange}
                />
              </div>
              <div className=" mt-5  mx-auto">
                <button type="submit"className=" btn btn-lg" id={style.sub}> Register! </button>
                </div>
            </form>
          </div>
    )
  }
}

export default Register
