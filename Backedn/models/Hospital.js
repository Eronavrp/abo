const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'hospitals',
  {
    idHospitals: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: Sequelize.STRING
    },
    Location: {
      type: Sequelize.STRING
    },
    SasiaAP: {
      type: Sequelize.INTEGER
    },
    SasiaAN: {
      type: Sequelize.INTEGER
    },    
    SasiaBP: {
      type: Sequelize.INTEGER
    },
    SasiaBN: {
      type: Sequelize.INTEGER
    },
    Sasia0P: {
      type: Sequelize.INTEGER
    },
    Sasia0N: {
      type: Sequelize.INTEGER
    },    
    SasiaABP: {
      type: Sequelize.INTEGER
    },
    SasiaABN: {
      type: Sequelize.INTEGER
    },
    photo: {
      type: Sequelize.STRING
    },
    latitude:{
      type:Sequelize.DOUBLE
    },
    longitude:{
      type:Sequelize.DOUBLE
    }

  },
  {
    timestamps: false
  }
)
