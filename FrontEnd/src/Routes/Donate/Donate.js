import React, { Component } from 'react';
import style from './Donate.module.css'
import DonateComp from './donateComp';
import Footer from './../Component/Footer';
import axios from 'axios'

export default class Donate extends Component {
  constructor() {
    super()
    this.state = {
      spitalet: []
    }
  }
  componentDidMount() {
    axios.get('http://localhost:8080/hospitals/hospital').then(res => {
      const info = res.data;
      this.setState({ spitalet: info });
    })
  }
  render() {
    console.log(this.state.spitalet.photo)
    
    return (
      <div>
        <div className={`${style.donate_box}`}>
          <div className="donate">
            <p id={`${style.donate}`}>Donate</p>
          </div>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-4">
                {this.state.spitalet.map((item, key) => item.SasiaAP < 25 ? <DonateComp hospital={item.Name} blood='A+' photo={item.photo} /> : '')}
                {this.state.spitalet.map((item, key) => item.SasiaAN < 25 ? <DonateComp hospital={item.Name} blood='A-' photo={item.photo} /> : '')}
                {this.state.spitalet.map((item, key) => item.SasiaBP < 25 ? <DonateComp hospital={item.Name} blood='B+' photo={item.photo} /> : '')}
                {this.state.spitalet.map((item, key) => item.SasiaBN < 25 ? <DonateComp hospital={item.Name} blood='B-' photo={item.photo} /> : '')}
                {this.state.spitalet.map((item, key) => item.SasiaABP < 25 ? <DonateComp hospital={item.Name} blood='AB+' photo={item.photo} /> : '')}
                {this.state.spitalet.map((item, key) => item.SasiaABN < 25 ? <DonateComp hospital={item.Name} blood='AB-' photo={item.photo} /> : '')}
                {this.state.spitalet.map((item, key) => item.Sasia0P < 25 ? <DonateComp hospital={item.Name} blood='0+' photo={item.photo} /> : '')}
                {this.state.spitalet.map((item, key) => item.Sasia0N < 25 ? <DonateComp hospital={item.Name} blood='0-' photo={item.photo} /> : '')}
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>

      // <div>
      //   <div className={`row `} id={style.main}>
      //     <div className={`col-sm-12 text-white  ${style.bar}`}>
      //       DONATE
      //     </div>
      //   </div>
      //   <div className={`row ${style.main}`}>
      //   {this.state.spitalet.map( (item,key) => item.SasiaAP<25? <DonateComp hospital={item.Name} blood='A+'/>:'')}
      //   {this.state.spitalet.map( (item,key) => item.SasiaAN<25? <DonateComp hospital={item.Name} blood='A-'/>:'')}
      //   {this.state.spitalet.map( (item,key) => item.SasiaBP<25? <DonateComp hospital={item.Name} blood='B+'/>:'')}
      //   {this.state.spitalet.map( (item,key) => item.SasiaBN<25? <DonateComp hospital={item.Name} blood='B-'/>:'')}
      //   {this.state.spitalet.map( (item,key) => item.SasiaABP<25? <DonateComp hospital={item.Name} blood='AB+'/>:'')}
      //   {this.state.spitalet.map( (item,key) => item.SasiaABN<25? <DonateComp hospital={item.Name} blood='AB-'/>:'')}
      //   {this.state.spitalet.map( (item,key) => item.Sasia0P<25? <DonateComp hospital={item.Name} blood='0+'/>:'')}
      //   {this.state.spitalet.map( (item,key) => item.Sasia0N<25? <DonateComp hospital={item.Name} blood='0-'/>:'')}
      //   </div>
      //   
      // </div>

    )
  }
}