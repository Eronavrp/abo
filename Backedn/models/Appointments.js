const Sequelize = require('sequelize')
const db = require('../database/db.js')
const user = require('./User')

module.exports = db.sequelize.define(
  'appointments',
  {
    idAppointments: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    Date:{
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    Users_idUsers:{
        type:Sequelize.INTEGER
    },
    Users_Appointments_idAppointments:{
        type:Sequelize.INTEGER
    },
    Hospitals_idHospitals:{
        type:Sequelize.INTEGER
    },
  },
  {
    timestamps: false
  }
)