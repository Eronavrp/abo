import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './Routes/Home/Home'
import Profile from './Routes/Profile/profile';
import Login from './Routes/Login/Login'
import Donate from './Routes/Donate/Donate'
import Calendar from './Routes/Calendar/Calendar'
import Place from './Routes/Place/Place'
import Signup from './Routes/Signup/Signup';
import First from './Routes/First/First';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/home" component={Home} />
        <Route exact path="/" component={First} />
        <Route exact path="/profile/:id" component={Profile} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/donate" component={Donate} />
        <Route exact path="/calendar/:id/:id" component={Calendar} />
        <Route exact path="/map/:id" component={Place} />
      </Switch>
    </Router>
  );
}

export default App;
