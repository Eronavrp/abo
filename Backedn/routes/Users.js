const express = require('express')
const users = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const User = require('../models/User')
users.use(cors())

process.env.SECRET_KEY = 'secret'

users.post('/register', (req, res) => {
  const today = new Date()
  const userData = {
    Name: req.body.Name,
    Surname: req.body.Surname,
    Email: req.body.Email,
    Password: req.body.Password,
    Gender:req.body.Gender,
    Blood:req.body.Blood,
    Addres:req.body.Addres,
    MedicalIssues:req.body.MedicalIssues,
    created: today
  }

  User.findOne({
    where: {
      Email: req.body.Email
    }
  })
    //TODO bcrypt
    .then(user => {
      if (!user) {
        bcrypt.hash(req.body.Password, 10, (err, hash) => {
          userData.Password = hash
          User.create(userData)
            .then(user => {
              res.json({ status: user.Email + 'Registered!' })
            })
            .catch(err => {
              res.send('error: ' + err)
            })
        })
      } else {
        res.json({ error: 'User already exists' })
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.post('/login', (req, res) => {
  User.findOne({
    where: {
      Email: req.body.Email
    }
  })
    .then(user => {
      if (user) {
        if (bcrypt.compareSync(req.body.Password, user.Password)) {
          let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
            expiresIn: 1440
          })
          res.send(token)
        }
      } else {
        res.status(400).json({ error: 'User does not exist' })
      }
    })
    .catch(err => {
      res.status(400).json({ error: err })
    })
})

users.get('/profile', (req, res) => {
  var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

  User.findOne({
    where: {
      idUsers: decoded.idUsers
    }
  })
    .then(user => {
      if (user) {
        res.json(user)
      } else {
        res.send('User does not exist')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

module.exports = users
