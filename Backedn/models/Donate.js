const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'donate',
  {
    idDonate: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    Donate:{type:Sequelize.INTEGER},
    Points:{type:Sequelize.INTEGER},
  },
  {
    timestamps: false
  }
)