import React, { Component } from 'react';
import style from './profile.module.css'

export default class Donations extends Component {

  render() {
    return (
      <div className={`col-11 mb-3 shadow text-white  ${style.donations}`}>
        <h5 className={`float-left `}>{this.props.name}</h5>
        <h5 className={`float-right ${style.hint}`}>{this.props.data}</h5>
      </div> 
    )
  }
}