import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { red } from '@material-ui/core/colors';
import { Link } from 'react-router-dom';
import style from './../Home.module.css'
import HospitalLogo from './../../../Images/hospital_logo.png'



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <div className={`btn btn-lg w-50 text-center mb-4`} id={`${style.buton}`} onClick={handleClickOpen}>Donate</div>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>        
        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close"> <CloseIcon /> </IconButton>
        <div className={`${style.box}`}>				
				<Link to='/calendar'> <button className={`${style.donate}`} id="donate"><span>Donate</span></button></Link>
        {/* actually waiting for the component from Altina */}
			</div>
      </Dialog>
    </div>
  );
}
