const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'leaderboard',
  {
    idLeaderboard: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    Users_idUsers:{type:Sequelize.INTEGER},
    Donate_idDonate:{type:Sequelize.INTEGER},
  },
  {
    timestamps: false
  }
)