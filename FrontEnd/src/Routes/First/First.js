import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../Images/logo_white.png';
import style from './First.module.css';

export default class First extends Component {
  render() {
    return (
      <div id={style.mainF}>
        <div >
          <img src={logo} className={`${style.logo}`} alt="logo" />
        </div>
        <div id={style.txtbg}>
          <p id={style.des} >You can donate to ones in need and get notified when you save a life</p>
        </div>
        <div className="row justify-content-center ">
          <Link to='/login'>
            <div className={`${style.label_1} `} >
              <h3 className={`align-middle`}  >LOG IN </h3>
            </div>
          </Link>
        </div>
        <div className="row justify-content-center ">
          <Link to='/signup'>
            <div className={`${style.label_2} `} >
              <h3>REGISTER</h3>
            </div>
          </Link>
        </div>
      </div>
    )
  }
}