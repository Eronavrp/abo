const express = require('express')
const router = express.Router()
const cors = require('cors')
const Blog = require('../models/Blog')

router.use(cors())

router.get('/blog', (req, res) => {
    Blog.findAll({}).then(
        Blog =>
         res.json(Blog))        
})

module.exports = router