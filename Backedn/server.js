var express = require('express')
var cors = require('cors')
var bodyParser = require('body-parser')
var app = express()
var port = process.env.PORT || 8080

app.use(bodyParser.json())
app.use(cors())
app.use(
  bodyParser.urlencoded({
    extended: false
  })
)

var Appointmens = require('./routes/Appointments')
var Blogs = require('./routes/Blog')
var Donate = require('./routes/Donate')
var Hospitals = require('./routes/Hospital')
var Leaderboard = require('./routes/Leaderboard')
var Users = require('./routes/Users')
var Users_has_donate = require('./routes/Users_has_donate')

app.use('/appointmens', Appointmens)
app.use('/blogs', Blogs)
app.use('/donate', Donate)
app.use('/hospitals', Hospitals)
app.use('/leaderboard', Leaderboard)
app.use('/users', Users)
app.use('/users_has_donate', Users_has_donate)

app.listen(port, function() {
  console.log('Server is running on port: ' + port)
})
