const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'user',
  {
    idUsers: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: Sequelize.STRING
    },
    Surname: {
      type: Sequelize.STRING
    },
    Gender:{
      type:Sequelize.STRING
    },
    Blood:{
      type:Sequelize.STRING
    },
    Email: {
      type: Sequelize.STRING
    },
    Password: {
      type: Sequelize.STRING
    },
    Addres:{
      type:Sequelize.STRING
    },
    MedicalIssues:{
      type:Sequelize.STRING
    },
    Appointments_idAppointments:{
      type:Sequelize.STRING,
    },
    created: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  },
  {
    timestamps: false
  }
)