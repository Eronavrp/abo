import axios from 'axios'

export const register = async newUser => {
    await axios
    .post('users/register', {
      Name: newUser.Name,
      Surname: newUser.Surname,
      Email: newUser.Email,
      Password: newUser.Password,
      Gender: newUser.Gender,
      Blood: newUser.Blood,
      Addres: newUser.Addres,
      MedicalIssues: newUser.MedicalIssues,
    })
  console.log('Registered')
}

export const login = async user => {
  try {
    const response = await axios
      .post('users/login', {
        Email: user.Email,
        Password: user.Password
      })
    localStorage.setItem('usertoken', response.data)
    return response.data
  }
  catch (err) {
    console.log(err)
  }
}