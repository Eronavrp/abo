import React, { Component } from 'react';
import { login } from './../../API/UserFunctions'
import logo from '../../Images/logo_white.png';
import style from './Login.module.css';

export default class Login extends Component {
	constructor() {
		super()
		this.state = {
		  Email: '',
		  Password: '',
		  errors: {}
		}	
		this.onChange = this.onChange.bind(this)
		this.onSubmit = this.onSubmit.bind(this)
	  }	
	  onChange(e) {
		this.setState({ [e.target.name]: e.target.value })
	  }
	  onSubmit(e) {
		e.preventDefault()	
		const user = {
		  Email: this.state.Email,
		  Password: this.state.Password
		}	
		login(user).then(res => {
		  if (res) {
			this.props.history.push(`/home`)
		  }
		})
	  }
  render() {
    return (
		<div className={` ${style.prov}`}>
			<div className="row justify-content-center">
				<img src={logo} className={`${style.logo}`} alt="logo" />
				<form onSubmit={this.onSubmit}>
					<input required  className={`${style.username} form-control`} type="email" name="Email" placeholder="Email"	value={this.state.Email} onChange={this.onChange}/>	
					<input required className={`${style.password}  form-control`} type="password" name="Password" placeholder="Password" value={this.state.Password} onChange={this.onChange}/>
					<button className={`${style.loginbtn}`} type="submit">Log In</button>			
				</form>
			</div>
		</div>      
    )
  }
}