import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import jwt_decode from 'jwt-decode'
import style from './Footer.module.css';
import home from './../../Images/footer/home.png';
import blood from './../../Images/footer/donate.png';
import users from './../../Images/footer/profile.png';


export default class Footer extends Component {  
  constructor() {
    super()
    this.state = {
      Name: '',
      Surname: ''
    }
  }
  componentDidMount() {
    const token = localStorage.usertoken
    const decoded = jwt_decode(token)
    this.setState({
      Name: decoded.Name,
      Surname: decoded.Surname })
  }

render() {
  return (
    <div className={`${style.bar}`}>
              <div className={`mt-1 row`}>
                  <div className={`col`}><Link to='/home'><img src={home} alt='' id={style.foto} /></Link></div>
                  <div className={`col`}><Link to='/donate'><img src={blood} alt='' id={style.foto} /></Link></div>
                  <div className={`col`}><Link to={`/profile/${this.state.Name}${this.state.Surname}`}><img src={users} alt='' id={style.foto} /></Link></div>
              </div>
          </div>     
    )
  }
  
}