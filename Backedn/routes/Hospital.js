const express = require('express')
const router = express.Router()
const cors = require('cors')
const Hospital = require('../models/Hospital')

router.use(cors())

router.get('/hospital', (req, res) => {
    Hospital.findAll({}).then(
        Hospital =>
         res.json(Hospital))        
})

module.exports = router