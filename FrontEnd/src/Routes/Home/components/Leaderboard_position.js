import React, { Component } from 'react';
import style from './Leaderboard.module.css'
import profilpic from './../../../Images/default-profil.jpg';

export default class LeaderboardPosition extends Component {

  render() {
    return (
        <tr>
          <td>
          <div className={style.rrethi}>
          <h4 id={style.nr}>{this.props.nr}</h4>
          </div>
          </td>
          <td>
            <div className={style.katrori}>
              <img src={profilpic} alt='' id={style.profilS} />
              <h4>{this.props.name}</h4>
            <h4 id={style.points}>{this.props.points}</h4>                            
            </div> 
          </td> 
        </tr>
    )
  }
}