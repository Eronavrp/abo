import React, { Component } from 'react';
import jwt_decode from 'jwt-decode';
import profilpic from './../../Images/default-profil.jpg';
import lapsi from './../../Images/lapsi.png'
import style from './profile.module.css';
import Donations from './donations';
import Footer from './../Component/Footer';

export default class Profile extends Component {
  logOut(e) {
    e.preventDefault()
    localStorage.removeItem('usertoken')
    this.props.history.push(`/`)
  }
  constructor() {
    super()
    this.state = {
      Name: '',
      Surname: '',
      Email: '',
      Gender:'',
      Blood:'',
      Addres:'',
      MedicalIssues:'',
      errors: {}
    }
  }

  componentDidMount() {
    const token = localStorage.usertoken
    const decoded = jwt_decode(token)
    this.setState({
      Name: decoded.Name,
      Surname: decoded.Surname ,
      Email: decoded.Email,
      Gender:decoded.Gender,
      Blood:decoded.Blood,
      Addres:decoded.Addres,
      MedicalIssues:decoded.MedicalIssues,
    })
  }

  render() {
    return (
      <div id={style.o}>
      <div className="ml-3 pt-2 mr-5" id={style.mainPB}>
        <div className="container">
          <div className="row">
           <div className="row">
              <div className="col-4 ">
              <img className="img-fluid rounded-circle border border-dark" src={profilpic} alt='profil'></img>
            </div>

            <div className="col-5 ">
              <h2 className="text-white text-bold">{this.state.Name}  {this.state.Surname }</h2>
              <h6 className="text-white">Edit Profile <img id={style.lapsi1} src={lapsi} alt=""></img></h6>              
            </div>

          </div>
            <div className="col-sm-12 pt-4">
              <h4 className={`float-left text-white`}>Gender:</h4>
              <h4 className={`float-left pl-3  ${style.hint}`}>{this.state.Gender}</h4>
            </div>

            <div className="col-sm-12">
              <h4 className={`float-left text-white`}>Blood type:</h4>
              <h4 className={`float-left pl-3 ${style.hint}`}>{this.state.Blood}</h4>
            </div>

            <div className="col-sm-12 mb-4">
              <h4 className={`float-left text-white`}>Medical Issues:</h4>
              <h4 className={`float-left pl-3 ${style.hint}`}>{this.state.MedicalIssues}</h4>
              <img id={style.lapsi} src={lapsi} alt=""></img>            
            </div>

            <h4 className={` mx-auto mb-3 text-white`}>Upcoming Appointments</h4>
            <Donations name='QKUK' data='April,4'/> 

            <h4 className={` mx-auto mb-3 text-white`}>Donations</h4>

            <Donations name='QKUK' data='June 10,2018'/> 
            <Donations name='Spitali Lindja' data='January 12,2019'/>
            <Donations name='Spitali Aloka' data='June 15,2019'/>           
            <Donations name='QKUK' data='January 15,2020'/>   
            
          </div>
          <a href="/" onClick={this.logOut.bind(this)} className="text-white" id={style.log}>
            Logout
          </a>
        </div>  
      </div>
      <Footer/>
      </div>
    )
  }
}
