import React, { Component } from 'react';
import style from './Calendar.module.css'
import CalendarReact from 'react-calendar';
import TimePicker from 'react-time-picker';
import { Link } from 'react-router-dom';
import Footer from './../Component/Footer';

export default class Calendar extends Component {
  state = {
    date: new Date(),
    time: '10:00',
  }
  
  onChange = date => this.setState({ date })

  onChange = time => this.setState({ time })

  render() {
    const { data } = this.props.location
    
    return (
        <div className={style.maindiv}>
          <div className={style.header}></div>
          <div>
            <h3 style={{ marginTop: 10, color: 'white' }}> Pick a date </h3>
            <div className={style.calendardiv}>
              <CalendarReact
                className={style.calendarDate}
                onChange={this.onChange}
                value={this.state.date}
              />
            </div>
            <h3 style={{ marginTop: 35 }}> Pick a time </h3>
            <div className={style.calendardiv}>
              <TimePicker
                className={style.calendarTime}
                onChange={this.onChange}
                value={this.state.time}
              />
            </div>
            <Link to={{pathname:`/map/${data}`,data:data}}> <button className={style.okbtn}> Next </button></Link>
          </div>
      <Footer/>
        </div>
    )
  }
}